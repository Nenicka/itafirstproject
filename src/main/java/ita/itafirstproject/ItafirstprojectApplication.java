package ita.itafirstproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItafirstprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItafirstprojectApplication.class, args);
    }

}
